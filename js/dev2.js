let containerBlogItems = 'content_config/blog_posts.json';
let containerProductItems = 'content_config/product_items.json';

$(document).ready(function(){
    callProceedingJSONs($(window).width());
    
})
$(window).resize(function(){
    $('#container__blog-carousel>.carousel-inner').empty();
    $('.container__blog-carousel-controls__large').empty();
    $('.container__products-furniture>.row').empty();
    callProceedingJSONs($(window).width())
})

const callProceedingJSONs = (wWidth) => {
    
    if(wWidth > 1199) {
        proceedBlogItemsJSON(containerBlogItems, 3);
        updateProductItemSection(containerProductItems, 9);
        // wrapFilters(false);
    } else if (wWidth > 767) {
        proceedBlogItemsJSON(containerBlogItems, 2);
        updateProductItemSection(containerProductItems, 4);
        // wrapFilters(true);
    } else {
        proceedBlogItemsJSON(containerBlogItems, 1);
        updateProductItemSection(containerProductItems, 3);
        // wrapFilters(true);
    }
}

// CREATE BLOGS

function proceedBlogItemsJSON(file, num){
    $.getJSON(file, function(json){
        let data = Array.from(json);
        carouselItemsUpdate(data, num);
        carouselIndicatorsUpdate(data.length / num);
    })
}


// CREATE PRODUCTS

function proceedProdItemsJSON(file, num){

}


// UPDATE NUMBER OF CAROUSEL ITEMS

function carouselIndicatorsUpdate(num){
    for (let i = 0; i < num; i++) {
        $('.container__blog-carousel-controls__large').append(
            `<div class="container__blog-carousel-controls__to-slide" data-slide-to="${i}"></div`
            );
        $()
        $('div.container__blog-carousel-controls__to-slide').removeClass('active');
    $(`div.container__blog-carousel-controls__to-slide[data-slide-to="0"]`).addClass('active');
    }
}


// UPDATE BLOG CAROUSEL ITEMS FROM JSON

function carouselItemsUpdate(itemData, num){
    let itemsNum = itemData.length / num;
    for (let i = 0; i < itemsNum; i++) {
        let itemCards = itemData.slice(i * num, i * num + num);

        $('#container__blog-carousel>.carousel-inner').append(`<div class="carousel-item" data-slide-id="${1}"><div class="row"></div></div>`);

        $.each(itemCards, function(iN, element){
            let cardContent = createBlogItemCardHTML(element)
            $(`<div class="col-12 col-md-6 col-xl-4">${cardContent}</div>`).appendTo(
                $(`#container__blog-carousel>.carousel-inner>.carousel-item:nth-of-type(${i+1})>.row`)
            );
        });
    }
    $('#container__blog-carousel>.carousel-inner>.carousel-item:first-of-type').addClass('active');
}


// CREATE BLOG CAROUSEL ITEM CARD FORM

function createBlogItemCardHTML(elem) {
    let itemDate = JSON.parse(elem.post_date);
    itemDate = new Date(itemDate);
    itemDate = formatDatePicker(itemDate);
    
    let itemCard = `
    <div class="card">
        <img src="${elem.post_img}" alt="" class="card-img-top">
        <div class="card-img-overlay d-flex justify-content-center align-items-end pb-0">
            <div class="card-text w-100 d-flex flex-row justify-content-between">
                <div class="p-3">
                    <i class="far fa-calendar-alt"></i>
                    ${itemDate.day}
                    ${itemDate.month}
                    ${itemDate.year}
                </div>
                <div class="p-3">
                    <i class="fas fa-comments"></i>
                    ${elem.post_comments} Comments
                </div>
            </div>
        </div>
        <div class="card-body text-center">
            <h5 class="card-title text-left">${elem.post_title}</h5>
            <p class="card-text text-left">${elem.post_text}</p>
            <a href="" class="btn btn-primary border border-white rounded-0 text-capitalize">read more</a>
        </div>
    </div>
    `
    return itemCard
}


// SET UP DATE FORMAT

const formatDatePicker = (d) => {
    mlist = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    return {
        day: d.getDate(), 
        month: mlist[parseInt(d.getMonth())], 
        year: d.getFullYear()}
}


// 


// BLOG CAROUSEL

$('#container__blog-carousel').carousel({
    interval: 200000
})
$('.container__blog-carousel-controls-next').click(function(){
    $('#container__blog-carousel').carousel('next')
})
$('.container__blog-carousel-controls-prev').click(function(){
    $('#container__blog-carousel').carousel('prev')
})

$('.container__blog-carousel-controls__large').click(function(e){
    let slideNum = e.target.dataset.slideTo
    if (!slideNum) return
    console.log(e.target);
    $('#container__blog-carousel').carousel(parseInt(slideNum));
    $('div.container__blog-carousel-controls__to-slide').removeClass('active');
    $(`div.container__blog-carousel-controls__to-slide[data-slide-to="${parseInt(slideNum)}"]`).addClass('active');
})

// NEWSLETTER CUSTOM PLACEHOLDER

$('.newsletter-form-input').focus(function(){
    $('.newsletter-form-label').hide();
});
$('.newsletter-form-input').blur(function(){
    console.log(`123: ${$(this).val()}`);
    
    if( $(this).val().length === 0 ) {
        $('.newsletter-form-label').show();
    } else {
        $('.newsletter-form-label').hide();
    }
});


// PRODUCT RATING FUNCTION

function formRating(rating){
    let stars = ""
    for (let i = 1; i < 6; i++) {
        if (i < (parseInt(rating) + 1)){
            stars += '<i class="fas fa-star"></i>';
        } else {
            stars += '<i class="far fa-star"></i>';
        }
    }
    return stars
}


// PRODUCT DATA ATTRIBUTES

function createDataAttrs(jsonItem) {
    let dataAttrs = ""
    for (const key in jsonItem) {
        if (jsonItem.hasOwnProperty(key)) {
            if(key.slice(0,2) == 'p_'){
                let element = jsonItem[key];
                let dataName = key.slice(2)
                dataAttrs += `data-item-${dataName}="${element}" `
            }
        }
    }

    return dataAttrs
}

// FURNITURE ITEM CARD FORM

function createProductCardHTML(productData) {
    let name = productData.p_name, img = productData.p_img, price = productData.p_price, rating = productData.p_star, category = productData.p_category
    let starRatingHTML = formRating(rating);
    // console.log(rating, starRatingHTML);
    
    let dataAttrs = createDataAttrs(productData);
    let prodHTML = 
        `
            <div class="container__products-furniture__item col-12 col-md-6 col-lg-4 d-flex justify-content-center align-items-center"
            ${dataAttrs}
            >
                <div class="container__products-furniture__item-inner">
                    <div class="container__products-furniture__item-inner_image-container position-relative">
                        <img src=${img} alt="f-1" class="container__products-furniture__item-inner__image">
                        <button 
                            class="container__products-furniture__item-inner_image-container__cart-supply position-absolute"
                            ${dataAttrs}
                            >
                                <i class="fas fa-shopping-basket"></i>
                                add to cart
                        </button>
                        <button 
                            class="container__products-furniture__item-inner_image-container__cart-overview position-absolute" 
                            data-toggle="modal" 
                            data-target="#product_overview_window" 
                            ${dataAttrs}
                            >
                                Quick View
                        </button>
                        <div class="container__products-furniture__item-inner_image-container__cart-mark position-absolute">
                                sale
                        </div>
                    </div>
                <div class="container__products-furniture__item-inner__descr text-center">
                    <div class="container__products-furniture__item-inner__item-name">
                        ${name}
                    </div>
                    <div class="container__products-furniture__item-inner__item-rating position-relative">
                        ${starRatingHTML}
                    </div>
                    <div class="container__products-furniture__item-inner__item-footer d-flex justify-content-between align-items-end">
                        <div class="container__products-furniture__item-inner__item-footer__buttons">
                            <button class="button-like">
                                    <i class="far fa-heart"></i>
                            </button>
                            <button class="button-exchange">
                                    <i class="fas fa-exchange-alt"></i>
                            </button>
                        </div>
                        <div class="container__products-furniture__item-inner__item-footer__price">
                            $${price}
                        </div>
                    </div>
                </div>
            </div>
            </div>
        `
    return prodHTML
}


// UPDATE PRODUCT SECTION

function updateProductItemSection(file, num){
    
    $.getJSON(file, function(json){
        let rawHTML = ""
        let data = Array.from(json)
        data.forEach((element, index) => {
            let raw = createProductCardHTML(element);
            if (index < parseInt(num)) {
                rawHTML += raw
            }
        });
        $('.container__products-furniture>.row').html(rawHTML);

        if($('.container__products-furniture__item').length >= data.length){
            $('.container__products__load-more').hide()
        }
    });


}


$('.container__products__load-more').click(function(){
    let idsLength = $('.container__products-furniture__item').length
    updateProductItemSection(containerProductItems, idsLength * 2);
})


// FURNITURE ITEMS MODAL WINDOW SHOW UP

$('#product_overview_window').on('show.bs.modal', function (event) {
    let button = $(event.relatedTarget);
    
    let itemData = {
        img: button.data('itemImg'),
        name: button.data('itemName'),
        price: button.data('itemPrice'),
        rating: button.data('itemStar')
    }
    console.log(itemData);
    let modal = $(this)
    
    modal.find('.product_overview-image img').attr('src', itemData.img);
    modal.find('.product_overview-description-title').text(itemData.name);
    modal.find('.product_overview-description__item-rating').html(function(){
        console.log(itemData.rating);
        
        return formRating(itemData.rating) + '<div class="product_overview-description__item-reviews">(0 reviews)</div>';
    })
})

$( function() {
    $( "#slider-range" ).slider({
        range: true,
        min: 0,
        max: 500,
        values: [ 10, 300 ],
        slide: function( event, ui ) {
            $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
        }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
    " - $" + $( "#slider-range" ).slider( "values", 1 ) );
} );
$( function() {
    $( ".container__products-filter__price__slider-range" ).slider({
        range: true,
        min: 0,
        max: 500,
        values: [ 10, 300 ],
        slide: function( event, ui ) {
            $( ".container__products-filter__price__amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
        }
    });
    $( ".container__products-filter__price__amount" ).val( "$" + $( ".container__products-filter__price__slider-range" ).slider( "values", 0 ) +
    " - $" + $( ".container__products-filter__price__slider-range" ).slider( "values", 1 ) );
} );